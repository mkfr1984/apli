import { LitElement, html } from 'lit-element';
import '../persona-ficha-listado/persona-ficha-listado.js'
import '../persona-form/persona-form.js'

class PersonaMain extends LitElement {


  static get properties() {
    return {
        people: {type: Array},
        showPersonForm: {type: Boolean}
    };
  }

  constructor() {

    super();

    this.people = [
        {
          name: "Ellen Ripley",
          yearsInCompany: 10,
          profile: "Lorem ipsum dolor sit amet.",
          photo: {
            src: "./img/persona1.jpg",
            alt: "Ellen Ripley"
          }
        }, {
          name: "Indiana Jones",
          yearsInCompany: 15,
          profile: "Lorem ipsum dolor sit.",
          photo: {
            src: "./img/persona2.jpg",
            alt: "Henry Jones Jr"
          }
        }, {
          name: "Turin",
          yearsInCompany: 5,
          profile: "lorem ipsum dolor sit amet usu ei laudem platonem.",
          photo: {
            src: "./img/persona3.jpg",
            alt: "Morgemil"
          }
        }, {
        name: "Abner Marsh",
        yearsInCompany: 7,
        profile: "lorem ipsum dolor sit amet usu ei laudem.",
        photo: {
          src: "./img/persona4.jpg",
          alt: "Abner"
          }
      }, {
        name: "Yennefer de Vengerberg",
        yearsInCompany: 3,
        profile: "lorem ipsum dolor sit amet usu ei.",
        photo: {
          src: "./img/persona5.jpg",
          alt: "Joshua"
        }
      }

    ];

    this.showPersonForm = false;
  }


  render() {
    return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
        <h2 class="text-center">Personas</h2>
        <div class="row" id="peopleList">
          <div class='row row-cols-1 row-cols-sm-4'>
          ${this.people.map(
            person => html`<persona-ficha-listado
            fname="${person.name}"
            yearsInCompany="${person.yearsInCompany}"
            profile="${person.profile}"
            .photo="${person.photo}"
            @delete-person="${this.deletePerson}"
            @info-person="${this.infoPerson}"></persona-ficha-listado>`               )}
          </div>
        </div>
        <div class="row">
            <persona-form id="personForm"
            class="d-none rounded border-primary"
            @persona-form-close="${this.personFormClose}"
            @persona-form-storage="${this.personFormStore}"></persona-form>
        </div>
      `;
  }

  updated(changedProperties) {
    console.log("updated");

    if (changedProperties.has("showPersonForm")){
      console.log("ha cambiado el valor de la propiedad showPersonForm en persona-main");

      if (this.showPersonForm === true){
        this.showPersonFormData();
      } else {
        this.showPersonList();
      }
    }

    if (changedProperties.has("people")) {
      console.log("Ha cambiado el valor de la propiedad People en persona main");

      this.dispatchEvent(new CustomEvent("updated-people",{
        detail: {
          people: this.people
        }}
      ));
    }
  }

  personFormClose() {
    console.log("personFormClose en persona-main");
    console.log("se ha cerrado el formulario persona");

    this.showPersonForm = false;
  }

  personFormStore(e){
    console.log("personStore");
    console.log("Se va a almacenar una persona");

    console.log("el nombre de la persona es " + e.detail.person.name);
    console.log("el nombre de la profile es " + e.detail.person.profile);
    console.log("el nombre de la years es " + e.detail.person.yearsInCompany);

    if (e.detail.editingPerson === true) {
      console.log("se va a actualizar la persona " + e.detail.person.name);

      this.people = this.people.map(
        person => person.name === e.detail.person.name
          ? person = e.detail.person : person);
    }else{
      console.log("se va a guardar una persona nueva");
      this.people = [...this.people, e.detail.person]
    }
    console.log("fin proceso guardado");

    this.showPersonForm = false;

  }

  showPersonList() {
    console.log("showPersonList");
    console.log("Mostrando el listado de personas.");
    this.shadowRoot.getElementById("personForm").classList.add("d-none");
    this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
  }

  showPersonFormData() {
    console.log("showPersonFormData");
    console.log("Mostrando el formulario de personas.");
    this.shadowRoot.getElementById("personForm").classList.remove("d-none");
    this.shadowRoot.getElementById("peopleList").classList.add("d-none");
  }

  deletePerson(e){
    console.log("deletePerson  en persona-main");
    console.log("se va a borrar la persona de nombre " + e.detail.name);

    this.people = this.people.filter(
      person => person.name != e.detail.name
    );
  }

  infoPerson(e) {
    console.log("infoPerson");
    console.log("Se ha pedido más info de la persona " + e.detail.name);

    let chosenPerson = this.people.filter(
      person => person.name === e.detail.name
    )

    let person = {};
    person.name = chosenPerson[0].name;
    person.profile = chosenPerson[0].profile;
    person.yearsInCompany = chosenPerson[0].yearsInCompany;
    person.photo = chosenPerson[0].photo;

    this.shadowRoot.getElementById("personForm").person = person;
    this.shadowRoot.getElementById("personForm").editingPerson = true;
    this.showPersonForm = true;
  }

}

customElements.define('persona-main', PersonaMain);
