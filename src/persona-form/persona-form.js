import { LitElement, html } from 'lit-element';

class PersonaForm extends LitElement {

  static get properties() {
    return {
      person: { type: Object },
      editingPerson: { type: Boolean }
    };
  }

  constructor() {

    super();

    this.resetFormData();


  }


  render() {
    return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
        <div>
          <form>
            <div class="form-group">
              <label>Nombre Completo</label>
              <input type="text" class="form-control" placeholder="nombre completo" @input="${this.updateName}"
                .value="${this.person.name}" ?disabled="${this.editingPerson}" />
            </div>
            <div class="form-group">
              <label>Perfil</label>
              <textarea class="form-control" placeholder="perfil" rows="5" @input="${this.updateProfile}"
                .value="${this.person.profile}"></textarea>
            </div>
            <div class="form-group">
              <label>Años en la empresa</label>
              <input type="text" class="form-control" placeholder="años en la empresa" @input="${this.updateYearsInCompany}"
                .value="${this.person.yearsInCompany}" />
            </div>
            <button @click="${this.goBack}" class="btn btn-primary"><strong>Atras</strong></button>
            <button @click="${this.storePerson}" class="btn btn-success"><strong>Guardar</strong></button>
          </form>
        </div>
      `;
  }

  updateName(e) {
    console.log("UpdateName");
    console.log("actualizando la propiedad name con el valor " + e.target.value);

    this.person.name = e.target.value;
  }

  updateProfile(e) {
    console.log("UpdateProfile");
    console.log("actualizando la propiedad profile con el valor " + e.target.value);

    this.person.profile = e.target.value;
  }

  updateYearsInCompany(e) {
    console.log("UpdateYearsInCompany");
    console.log("actualizando la propiedad years con el valor " + e.target.value);

    this.person.yearsInCompany = e.target.value;
  }


  goBack(e) {
    console.log("goBack");
    e.preventDefault();

    this.dispatchEvent(new CustomEvent("persona-form-close", {}));

    this.resetFormData();
  }

  resetFormData() {
    this.person = {};
    this.person.name = "";
    this.person.profile = "";
    this.person.yearsInCompany = "";

    this.editingPerson = false;
  }

  storePerson(e) {
    console.log("storePersona");
    console.log("Se va a guardar una persona");

    e.preventDefault();

    console.log("la propiedad name de la persona es " + this.person.name);
    console.log("la propiedad profile de la persona es " + this.person.profile);
    console.log("la propiedad years de la persona es " + this.person.yearsInCompany);
    console.log(this.person.photo);

    if (this.person.photo == undefined) {
      this.person.photo = {
        "src": "./img/persona.jpg",
        "alt": "Persona"
      }
    }


    this.dispatchEvent(new CustomEvent("persona-form-storage", {
      detail: {
        person: {
          name: this.person.name,
          profile: this.person.profile,
          yearsInCompany: this.person.yearsInCompany,
          photo: this.person.photo
        },
        editingPerson: this.editingPerson
      }
    }));
  }


}

customElements.define('persona-form', PersonaForm);
