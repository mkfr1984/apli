import { LitElement, html } from 'lit-element';

class FichaPersona extends LitElement {

  static get properties() {
    return {
      name: { type: String },
      yearsInCompany: { type: Number },
      personInfo: { type: String }
    };
  }

  constructor() {

    super();
    console.log("constructor");
    this.name = "Prueba nombre";
    this.yearsInCompany = 12;
    this.personInfo = this.calcularPersonInfo();
  }

  calcularPersonInfo() {
    if (this.yearsInCompany >= 7) {
      return "lead";
    } else if (this.yearsInCompany >= 5) {
      return "senior";
    } else if (this.yearsInCompany >= 3) {
      return "team";
    } else {
      return "junior";
    }
  }

  updated(changedProperties) {
    console.log("updated")

    changedProperties.forEach((oldValue, propName) => {
      console.log(`${propName} changed. oldValue: ${oldValue}`);
    });

    if (changedProperties.has("name")) {
      console.log("Propiedad name ha cambiado de valor, atnerior era " +
        changedProperties.get("name") + ",el nuevo es " + this.name);
    }
  }

  render() {
    return html`
        <div>
          <label>Nombre Completo</label>
          <input type="text" id="fname" name="fname" value="${this.name}" @input="${this.updateName}"></input>
          <br />
          <label>Años en la empresa</label>
          <input type="text" name="yearsInCompany" value="${this.yearsInCompany}" @input="${this.updateyearsInCompany}"></input>
          <br />
          <input type="text" value="${this.personInfo}" disabled></input>
        </div>
      `;
  }

  updateName(e) {
    console.log("updateName");
    this.name = e.target.value;
  }

  updateyearsInCompany(e) {
    console.log("updateyearsInCompany");
    this.yearsInCompany = e.target.value;
    this.personInfo = this.calcularPersonInfo();
  }
}

customElements.define('ficha-persona', FichaPersona);
